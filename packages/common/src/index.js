const async = require('./async');
const callOnce = require('./call-once');
const isObject = require('./is-object');
const stripHtml = require('./strip-html');

module.exports = {
  async,
  callOnce,
  isObject,
  stripHtml,
};
