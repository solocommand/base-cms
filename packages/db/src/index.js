const BaseDB = require('./basedb');
const MongoDB = require('./mongodb');

module.exports = {
  BaseDB,
  MongoDB,
};
